<?php

    define('BOX_ERROR', 'Este campo es requerido');
    
    // Tambien nos sirve como trigger del formulario
    $errors = [];

    // Iniciamos con campos vacios
    $username = '';
    $email = '';
    $password = '';
    $password_confirm = '';
    $cv_url = '';
    
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {

        // echo '<pre>';
        //    var_dump($_POST);
        // echo '</pre>';

        $username = postData('username');
        $email = postData('email');
        $password = postData('password');
        $password_confirm = postData('password_confirm');
        $cv_url = postData('cv_url');

                if (!$username) {

                    $errors['username'] = BOX_ERROR;
                } else if (strlen($username) < 6 || strlen($username) > 16) {

                      $errors['username'] = 'El usuario debe tener entre 6 y 16 caracteres';  

                }
            
                if (!$email) {
            
                    $errors['email'] = BOX_ERROR;
            
                } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {

                        $errors['email'] = 'Por favor ingresa un email valido';                    
                }
            
                if (!$password) {
            
                    $errors['password'] = BOX_ERROR;
            
                }
            
                if (!$password_confirm) {
            
                    $errors['password_confirm'] = BOX_ERROR;
            
                }

                // Evaluamos si ambos campos existen y comparamos si son iguales
                if ($password && $password_confirm && strcmp($password, $password_confirm) !== 0) {

                        $errors['password_confirm'] = 'Este campo debe ser el mismo que password';

                }
            
                if (!$cv_url) {
            
                    $errors['cv_url'] = BOX_ERROR;
            
                } else if ($cv_url && !filter_var($cv_url, FILTER_VALIDATE_URL)) {

                    $errors['cv_url'] = 'Debes ingresar una URL valida.';
                }


                if (empty($errors)) {

                    echo '<h2 class="display-5">' . 'Formulario exitoso!' . '</h2>';
                }

        // echo '<pre>';
        //     var_dump($username, $email, $password, $password_confirm, $cv_url);
        // echo '</pre>';

    }

    function postData($field) {

        // Para que funcione el operador '??' es preciso juntar el '='
        $_POST[$field] ??= '';

        return htmlspecialchars(stripcslashes($_POST[$field]));
        // return empty($_POST[$field]) ? '' : htmlspecialchars(stripcslashes($_POST[$field]));
    }
            
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <title>Form Validation</title>
</head>
<body>


       
    <div class="container">
                <form method='post'>
                      <div class="row">
                          <div class="col">
                              <div class="form-group">
                                  <label for="username">Username</label>
                                  <input type="text" class="form-control <?= isset($errors['username']) ? 'is-invalid' : ''; ?>" name="username" placeholder="Username" value="<?= $username; ?>">
                                  <small class="form-text text-muted">Minimo: 6 y maximo 16 caracteres.</small>
                                  <div class="invalid-feedback"><?=  $errors['username'] ?? ''; ?></div>
                              </div>
                          </div>
                          <div class="col">
                              <div class="form-group">
                                  <label for="email">Email</label>
                                  <input type="email" class="form-control <?= isset($errors['email']) ? 'is-invalid' : ''; ?>" name="email" placeholder="Email" value="<?= $email; ?>">
                                  <div class="invalid-feedback"><?=  $errors['email'] ?? ''; ?></div>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col">
                              <div class="form-group">
                                  <label for="password">Password</label>
                                  <input type="password" class="form-control <?= isset($errors['password']) ? 'is-invalid' : ''; ?>" name="password" placeholder="Password" value="<?= $password; ?>">
                                  <div class="invalid-feedback"><?=  $errors['password'] ?? ''; ?></div>
                              </div>
                          </div>
                          <div class="col">
                              <div class="form-group">
                                  <label for="password_confirm">Repeat password</label>
                                  <input type="password" class="form-control <?= isset($errors['password_confirm']) ? 'is-invalid' : ''; ?>" name="password_confirm" placeholder="Password repeat" value="<?= $password_confirm; ?>">
                                  <div class="invalid-feedback"><?=  $errors['password_confirm'] ?? ''; ?></div>
                              </div>
                          </div>
                          <div class="col">
                                <div class="form-group">
                                    <label for="cv_url">Your CV link</label>
                                    <input type="text" class="form-control <?= isset($errors['cv_url']) ? 'is-invalid' : ''; ?>" name="cv_url" placeholder="https://example.com/my-cv" value="<?= $cv_url; ?>">
                                  <div class="invalid-feedback"><?=  $errors['cv_url'] ?? ''; ?></div>
                                </div>
                          </div>  
                      </div>  
                      <div class="row">
                          <div class="col">
                              <div class="form-group">
                                <button class="btn btn-primary" name="register">Register</button>
                              </div>
                          </div>                    
                      </div>
                </form>

    </div>          
                
          

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>     
</body>
</html>